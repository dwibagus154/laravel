<?php

class Animal
{
    public $legs,
        $cold_blooded,
        $name;

    public function __construct($name, $legs = 2, $cold_blooded = 'false')
    {
        $this->name = $name;
        $this->legs = $legs;
        $this->cold_blooded = $cold_blooded;
    }
}

$sheep = new Animal("shaun");
echo $sheep->name; // "shaun"
echo $sheep->legs; // 2
echo $sheep->cold_blooded; // false
