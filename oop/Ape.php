<?php

require_once("Animal.php");

class Ape extends Animal
{
    public function __construct($name, $legs = 2, $cold_blooded = 'false')
    {
        parent::__construct($name, $legs = 2, $cold_blooded = 'false');
    }

    public function yell()
    {
        echo "Auoo";
    }
}

$sungokong = new Ape("kera sakti");
$sungokong->yell();// "Auooo"