<?php

require_once("Animal.php");

class Frog extends Animal
{
    public function __construct($name, $legs = 4, $cold_blooded = 'false')
    {
        parent::__construct($name, $legs = 4, $cold_blooded = 'false');
    }

    public function jump()
    {
        echo "hop hop";
    }
}

$kodok = new Frog("buduk");
$kodok->jump(); // "hop hop"