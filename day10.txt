soal 1.  create database myshop;

soal 2. 

MariaDB [myshop]> create table users(
    -> id int primary key auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255)
    -> );
Query OK, 0 rows affected (0.024 sec)

MariaDB [myshop]> create table items(
    -> id int primary key auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int,
    -> stock int,
    -> category_id int
    -> );
Query OK, 0 rows affected (0.028 sec)

MariaDB [myshop]> create table categories(
    -> id int primary key auto_increment,
    -> name varchar(255)
    -> );
Query OK, 0 rows affected (0.027 sec)

soal 3. 

MariaDB [myshop]> insert into users values ('', 'John Doe', 'john@doe.com', 'john123');
Query OK, 1 row affected, 1 warning (0.033 sec)
MariaDB [myshop]> insert into users values ('', 'Jane Doe', 'jane@doe.com', 'jenita123');
Query OK, 1 row affected, 1 warning (0.004 sec)

insert into categories values ('', 'gadget'), ('', 'cloth'),('', 'men'),('', 'women'),('', 'branded');

insert into items values ('', 'Sumsang b50', 'hape keren merek sumsang', '4000000', '100', '1'), ('', 'Uniklooh', 'baju keren dari brand ternama', '500000', '50', '2'), ('', 'IMHO Watch', 'jam tangan anak yang jujur banget', '2000000', '10', '1');

soal.4 
a. select users.id, users.name, users.email from users;
b.1 select * from items where items.price > 1000000;
b.2 select * from items where name like '%watch%'
c. select items.* , categories.name from items inner join categories on items.category_id = categories.id;

soal 5.

update items set price = 2500000 where name = 'Sumsang b50';