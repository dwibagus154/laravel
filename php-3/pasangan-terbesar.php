<?php
function pasangan_terbesar($angka)
{
    // kode di sini
    $string = strval($angka);
    $a = 0;
    for ($i = 0; $i < strlen($string) - 1; $i++) {
        $b = intval($string[$i] . $string[$i + 1]);
        if ($b >= $a) {
            $a = $b;
        }
    }
    return $a;
}

// TEST CASES
echo pasangan_terbesar(641573); // 73
echo "<br>";
echo pasangan_terbesar(12783456); // 83
echo "<br>";
echo pasangan_terbesar(910233); // 91
echo "<br>";
echo pasangan_terbesar(71856421); // 85
echo "<br>";
echo pasangan_terbesar(79918293); // 99
