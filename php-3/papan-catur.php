<?php

function papan_catur($angka)
{
    // tulis kode di sini
    $output = "";
    for ($i = 0; $i < $angka; $i++) {
        $a = "";
        if ($i % 2 == 0) {
            for ($j = 0; $j < 2 * $angka; $j++) {

                if ($j % 2 == 0) {
                    $a .= "#";
                } else {
                    $a .= " ";
                }
            }
        } else {
            for ($j = 0; $j < 2 * $angka - 1; $j++) {
                if ($j % 2 == 0) {
                    $a .= " ";
                } else {
                    $a .= "#";
                }
            }
        }
        $output .= $a;
        $output .= "<br>";
    }
    return $output;
}

// TEST CASES
echo papan_catur(4);
echo "<br>";
/*
# # # #
 # # #
# # # #
 # # #
 */

echo papan_catur(8);
/* 
# # # # # # # #
 # # # # # # # 
# # # # # # # #
 # # # # # # # 
# # # # # # # #
 # # # # # # #
# # # # # # # #
 # # # # # # #

echo papan_catur(5) 
/*
# # # # #
 # # # #
# # # # #
 # # # # 
# # # # #
*/