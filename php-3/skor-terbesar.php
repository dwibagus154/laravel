<?php
function skor_terbesar($arr)
{
    $new = [];
    //kode di sini
    $course = [$arr[0]["kelas"]];
    for ($i = 1; $i < count($arr); $i++) {
        $b = true;
        $c = $i;
        for ($j = 0; $j < count($course); $j++) {
            if ($course[$j] == $arr[$i]["kelas"]) {
                $b = false;
            }
        }
        if ($b) {
            array_push($course, $arr[$c]["kelas"]);
        }
    }

    for ($i = 0; $i < count($course); $i++) {
        $nilai = 0;
        $indeks = 0;
        for ($j = 0; $j < count($arr); $j++) {
            if ($course[$i] == $arr[$j]["kelas"]) {
                if ($nilai < $arr[$j]["nilai"]) {
                    $nilai = $arr[$j]["nilai"];
                    $indeks = $j;
                }
            }
        }
        $new1 = [$arr[$indeks]["kelas"] => $arr[$indeks]];
        $new = array_merge($new, $new1);
    }




    return $new;
}

// TEST CASES
$skor = [
    [
        "nama" => "Bobby",
        "kelas" => "Laravel",
        "nilai" => 78
    ],
    [
        "nama" => "Regi",
        "kelas" => "React Native",
        "nilai" => 86
    ],
    [
        "nama" => "Aghnat",
        "kelas" => "Laravel",
        "nilai" => 90
    ],
    [
        "nama" => "Indra",
        "kelas" => "React JS",
        "nilai" => 85
    ],
    [
        "nama" => "Yoga",
        "kelas" => "React Native",
        "nilai" => 77
    ],
];

print_r(skor_terbesar($skor));
/* OUTPUT
  Array (
    [Laravel] => Array
              (
                [nama] => Aghnat
                [kelas] => Laravel
                [nilai] => 90
              )
    [React Native] => Array
                  (
                    [nama] => Regi
                    [kelas] => React Native
                    [nilai] => 86
                  )
    [React JS] => Array
                (
                  [nama] => Indra
                  [kelas] => React JS
                  [nilai] => 85
                )
  )
*/
