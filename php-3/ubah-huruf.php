<?php
function ubah_huruf($string)
{
    //kode di sini
    $huruf = "abcdefghijklmnopqrstuvwxyz";
    $new = "";

    for ($i = 0; $i < strlen($string); $i++) {
        for ($j = 0; $j < 26; $j++) {
            if ($string[$i] == $huruf[$j]) {
                $new .= $huruf[$j + 1];
            }
        }
    }
    return $new;
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo "<br>";
echo ubah_huruf('developer'); // efwfmpqfs
echo "<br>";
echo ubah_huruf('laravel'); // mbsbwfm
echo "<br>";
echo ubah_huruf('keren'); // lfsfo
echo "<br>";
echo ubah_huruf('semangat'); // tfnbohbu
