<?php


function palindrome_angka($angka)
{
    // tulis kode di sini
    $val = true;
    while ($val) {
        $angka +=  1;
        $string = strval($angka);
        $a = "";
        $panjang = strlen($string);
        for ($i = $panjang - 1; $i > -1; $i--) {
            $a = $a . $string[$i];
        }
        if ($string == $a) {
            $val = false;
        } else {
            $val = true;
        }
    }
    return $angka;
}
// echo (strval(1));
// TEST CASES
echo palindrome_angka(8); // 9
echo ("<br>");
echo palindrome_angka(10); // 11
echo ("<br>");
echo palindrome_angka(117); // 121
echo ("<br>");
echo palindrome_angka(175); // 181
echo ("<br>");
echo palindrome_angka(1000); // 1001

// soal 2
