<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/about', function () {
//     $nama = 'dwibagus';
//     return view('about', ['nama' => $nama]);
// });

Route::get('/', "PagesController@index");
Route::get('/about', "PagesController@about");
Route::get('/mahasiswa', "MahasiswaController@index");
Route::post('/student', "StudentsController@store");
Route::get('/student', "StudentsController@index");


Route::get('/student/add', "StudentsController@create");
Route::get('/student/{student}', "StudentsController@show");
Route::delete('/student/{student}', "StudentsController@destroy");


// sanber
// tugas12
// Route::get('/', "HomeController@index");
Route::get('/registrasi', "AuthController@index");
Route::post('/registrasi/welcome', "AuthController@welcome");

// tugas13 
// Route::get('/', "Tugas13@index");
Route::get('/data-tables', "Tugas13@data");
