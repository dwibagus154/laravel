@extends('../layouts.app')

@section('title', 'About')


@section('content')


<div class="row">
    <div class="col-lg-6">
        <div class="card" style="width: 18rem;">
            <div class="card-body">
                <h5 class="card-title">{{$student["nama"]}}</h5>
                <h6 class="card-subtitle mb-2 text-muted">{{$student["jurusan"]}}</h6>
                <p class="card-text">{{$student["nim"]}} dan {{$student["email"]}}</p>
                <form action="{{$student['id']}}" method="post" class="d-inline">
                    @method("delete")
                    @csrf
                    <button type="submit" class="badge badge-warning">Hapus</button>
                </form>
                <a href="" class="badge badge-danger">Edit</a>
                <a href="/student" class="card-link">kembali</a>
            </div>
        </div>
    </div>
</div>

@endsection