@extends('../layouts.app')

@section('title', 'About')


@section('content')

@if (session('status'))
<div class="alert alert-success">
    {{ session('status') }}
</div>
@endif

<a href="/student/add" class="badge badge-primary" style="margin-top: 20px;">Tambah data mahasiswa</a>

@foreach ($student as $s)
<div class="row">
    <div class="col-lg-6">
        <ul class="list-group">
            <li class="list-group-item d-flex justify-content-between align-items-center">
                {{$s["nama"]}}
                <a href="student/{{ $s['id'] }}"><span class="badge badge-primary badge-pill">detail</span></a>
            </li>
        </ul>
    </div>
</div>
@endforeach







@endsection