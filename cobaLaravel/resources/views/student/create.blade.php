@extends('../layouts.app')

@section('title', 'About')


@section('content')


<h1>Create New Students</h1>

<form action="/student" method="post">
    @csrf
    <!-- guna csrf biar tidak ada orang yang bisa mengakses form dari luar, sehingga halaman tidak page expired -->
    <div class="form-group">
        <label for="nama">Name</label>
        <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama" name="nama" placeholder="masukkan nama" value="{{old('nama')}}">
        @error('nama')
        <div class="invalid-feedback">
            fill the name
        </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="email">Email address</label>
        <input type="text" class="form-control @error('email') is-invalid @enderror" id="email" name="email" placeholder="masukkan email">
    </div>
    <div class="form-group">
        <label for="jurusan">Jurusan</label>
        <input type="text" class="form-control @error('jurusan') is-invalid @enderror" id="jurusan" name="jurusan" placeholder="masukkan jurusan">
    </div>
    <div class="form-group">
        <label for="NIM">NIM</label>
        <input type="text" class="form-control @error('nim') is-invalid @enderror" id="NIM" name="nim" placeholder="masukkan NIM">
    </div>
    <button type="submit" class="btn btn-primary">Create</button>
</form>

@endsection