<!DOCTYPE html>
<html>

<head>
    <title>Halaman Form</title>
</head>

<body>
    <h2><strong>Buat Account Baru</strong></h2>
    <p><strong>Sign Up Form</strong></p>
    <form action="{{url('/registrasi/welcome')}}" method="post">
        @csrf
        <div>
            <label for="name1">First Name: </label>
            <br><br>
            <input type="text" name="name1" id="name1">
        </div>
        <br>
        <div>
            <label for="name2">Last Name: </label>
            <br><br>
            <input type="text" name="name2" id="name2">
        </div>
        <br>
        <p>Gender:</p>
        <div>
            <input type="radio" name="box" id="box1">
            <label for="box1">male</label>
            <br>
            <input type="radio" name="box" id="box2">
            <label for="box2">female</label>
            <br>
            <input type="radio" name="box" id="box3">
            <label for="box3">other</label>
        </div>
        <p>Nasionality:</p>
        <select name="Nasionality">
            <option value="Indonesia">Indonesia</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Singapura">Singapura</option>
            <option value="Thailand">Thailand</option>
        </select>
        <p>Language Spoken: </p>
        <div>
            <input type="checkbox" name="ls" id="ls1">
            <label for="ls1">Bahasa Indonesia</label>
            <br>
            <input type="checkbox" name="ls" id="ls2">
            <label for="ls2">English</label>
            <br>
            <input type="checkbox" name="ls" id="ls3">
            <label for="ls3">other</label>
        </div>

        <div>
            <label>Bio:</label>
            <br>
            <br>
            <textarea rows="10" cols="30"></textarea>
        </div>
        <button type="submit">Sign Up</button>
    </form>

</body>

</html>