<?php

namespace App\Http\Controllers;

use App\Student;
use Illuminate\Http\Request;

class StudentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $student = Student::all();
        return view("student.index", ['student' => $student]);
    }

    /**
     * Show thev form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("student.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $student = new Student;
        // $student->nama = $request->nama;
        // $student->email = $request->email;
        // $student->jurusan = $request->jurusan;
        // $student->nim = $request->nim;
        // $student->save();

        // cara 2
        // Student::create([
        //     'nama' => $request->nama,
        //     'email' => $request->email,
        //     'jurusan' => $request->jurusan,
        //     'nim' => $request->nim
        // ]);

        // cara3 
        $validatedData = $request->validate([
            'nama' => 'required',
            'nim' => 'required|size:8',
            'email' => "required",
            'jurusan' => 'required'
        ]);

        $student = new Student;
        $student->nama = $request->nama;
        $student->nim = $request->nim;
        $student->email = $request->email;
        $student->jurusan = $request->jurusan;
        $student->save();

        // Student::create($request->all());

        return redirect('/student')->with('status', 'Data updated!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        return view("student.show", compact('student'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        //
        Student::destroy($student->id);
        return redirect('/student')->with('status', 'Data deleted!');
    }
}
