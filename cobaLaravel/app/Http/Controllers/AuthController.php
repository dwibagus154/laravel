<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index()
    {
        return view("sanber.tugas12.form");
    }
    public function welcome(Request $request)
    {
        $nama1 = $request->name1;
        $nama2 = $request->name2;
        return view("sanber.tugas12.welcome", ['nama1' => $nama1, 'nama2' => $nama2]);
    }
}
